## vim: syntax=perl

requires "IO::Prompt";
requires "Math::Random";
requires "Mouse";
requires "Pod::Perldoc";

on test => sub {
    requires "Test::More";
};
