# Project Name - Bowling-Game #

Bowling Game Simulation

# SETUP #
--------------------------------------------------------------
## Setup your system with Docker and Vagrant ##

### Install Docker ###

p.s. If you already have docker, skip to next.

 sudo wget -q0- https://get.docker.com|sh
 
 sudo adduser $USER docker
 
 echo "export VAGRANT_DEFAULT_PROVIDER=docker" >> $HOME/.bashrc;
 
 export VAGRANT_DEFAULT_PROVIDER=docker
 
 sudo reboot
 
### Install Vagrant ###

p.s. If you already have vagrant, or use docker composer then skip to next.

Download the latest version from https://www.vagrantup.com/downloads.html

 sudo apt-get gdebi -y
 
 wget https://dl.bintray.com/mitchellh/vagrant/vagrant_1.7.2_x86_64.deb -cO vagrant.deb
 
 sudo gdebi vagrant.deb --no

### Add ./bin and ./tools to PATH ###

p.s. If you have already done that, skip this one. do not over done.

 echo "export PATH=bin:tools:$PATH" >> ~/.bashrc

=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-===-=

## Runing ##

### Install dependancies  ###

 carton install

### HOW TO PLAY ###

 Run `carton exec bin/bowling-game.pl`

-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-===-=

# Developers #

 * Michael Vu <email@michael.vu>

# License #

None
