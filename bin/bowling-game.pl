#!/usr/bin/env perl

use strict;
use warnings;
use lib "lib";
use Bowling::Game;

=head1 NAME

Bowling Game Simlation

=head1 HOW TO PLAY

Run `carton exec bin/bowling-game.pl`

=head1 RULES

=head2 NORMAL POINT

The player whose ball is nearest to the jack gets one point.

=head2 EXTRA POINT

If the second nearest ball is also theirs they get an extra point.

=head2 THIRD POINT

Additionally, if the third nearest ball is also theirs then they get a third point.

=cut

sub main {
    my $game = Bowling::Game->new;

    _greeting("WELCOME TO PLAY - BOWLING GAME SIMULATION");

    ## Question Time
    $game->ask_num_of_players;
    $game->ask_num_of_balls;
    $game->ask_num_of_rounds;

    ## Setup Game
    $game->set_field;

    printf "Field size: %d\n", $game->_field_size;

    $game->set_jack_position;

    printf "Jack location of the field: %d\n", $game->_jack_location;

    $game->set_rounds;

    ## Play Game
    while ( my $round = $game->next_round ) {
        printf ">> Round %d\n", $round->id;
        while ( my $player = $round->next_player ) {
            my $distance = $player->roll_ball;
            printf ">> %s rolled ball and the distance is %d\n", $player->name,
              $distance;
        }
        sleep 1;
    }

    print "\n --- GAME OVER --- \n";

    ## Result Aggregation
    my @highest_score_players = $game->result_of_each_players;

    my ( $winners, @other_players ) = _who_is_winner(@highest_score_players);

    _greeting("PLAYERS RESULT");

    if ($winners) {
        _print_result( winners => @$winners );
        _print_result( others  => @other_players );
    }
    else {
        print "No one win in this game.\n";
        _print_result( others => @$winners, @other_players );
    }
}

if ( grep { /help/i } @ARGV ) {
    exec "perldoc", "$0";
    exit;
}

caller or main();

sub _print_result {
    my $won     = shift;
    my @players = @_;
    foreach my $player (@players) {
        if ( $won eq "winners" ) {
            printf ">> %s is a WINNER, who has got %d points\n",
              @$player{qw{name points}};
        }
        else {
            printf ".. %s has got %d points\n", @$player{qw{name points}};
        }
    }
}

sub _who_is_winner {
    my @players              = @_;
    my %player_mapped_points = ();

    foreach my $player (@players) {
        my $points = $player->{points};
        push @{ $player_mapped_points{$points} ||= [] }, $player;
    }

    my ( $highest_points, @lower_points ) =
      sort { $b <=> $a } keys %player_mapped_points;

    ## no one got any points
    return
      if $highest_points == 0;

    my @winners = @{ $player_mapped_points{$highest_points} };

    ## everyone got the same points
    return
      if ~~ @players == ~~ @winners;

    my @others = map { @{ $player_mapped_points{$_} } } @lower_points;

    return ( \@winners, @others );
}

sub _greeting {
    my $greeting = shift;

    printf "%s\n", "-" x ( length($greeting) + 2 );
    print " $greeting \n";
    printf "%s\n", "-" x ( length($greeting) + 2 );
}
