use strict;
use warnings;
use Test::More;
use_ok "Bowling::Game";
use_ok "Bowling::Round";
use_ok "Bowling::Player";
done_testing;
exit 0;
