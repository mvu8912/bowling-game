package Bowling::Round;

use strict;
use warnings;
use Bowling::Player;

=head1 NAME

Bowling::Round - Bowling Game Rounder

=cut

use Mouse;

has id => (
    is  => "ro",
    isa => "Int",
);

has num_of_players => (
    is  => "rw",
    isa => "Int",
);

has num_of_balls => (
    is  => "rw",
    isa => "Int",
);

has players => (
    is         => "rw",
    isa        => "ArrayRef",
    lazy_build => 1,
);

sub _build_players {
    my $self           = shift;
    my $num_of_players = $self->num_of_players;
    my $num_of_balls   = $self->num_of_balls;
    my @players        = map {
        Bowling::Player->new(
            name          => "Player $_",
            _num_of_balls => $num_of_balls
          )
    } ( 1 .. $num_of_players );
    return \@players;
}

has _current_player => (
    is      => "rw",
    isa     => "Int",
    default => -1,
);

has _current_rolled_num_of_balls => (
    is      => "rw",
    isa     => "Int",
    default => 0,
);

no Mouse;

sub next_player {
    my $self = shift;

    ## Round Iterator
    my $rolled_balls = $self->_current_rolled_num_of_balls + 1;

    if ( $rolled_balls <= ( $self->num_of_balls * $self->num_of_players ) ) {
        ## Remember the current round id
        $self->_current_rolled_num_of_balls($rolled_balls);
    }
    else {
        ## Finish One round
        return;
    }

    ## Player Iterator
    my $current_player = $self->_current_player + 1;
    my $player         = $self->players->[$current_player];

    ## If the round is still valid. Reset the player iterator
    if ( !$player ) {
        $current_player = 0;
        $player         = $self->players->[$current_player];
    }

    ## Remember the current player id
    $self->_current_player($current_player);

    return $player;
}

1;

