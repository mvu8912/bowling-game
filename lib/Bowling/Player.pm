package Bowling::Player;

use strict;
use warnings;
use Math::Random qw( random_uniform_integer );

=head1 NAME

Bowling::Player - The Player in the Bowling Game Simulation

=cut

use Mouse;

has name => (
    is       => "rw",
    isa      => "Str",
    required => 1,
);

has _current_ball => (
    is      => "rw",
    isa     => "Int",
    default => -1,
);

has _num_of_balls => (
    is  => "rw",
    isa => "Int",
);

has _balls => (
    is         => "ro",
    isa        => "ArrayRef[Int]",
    lazy_build => 1,
);

sub _build__balls {
    my $self = shift;
    return [ 1 .. $self->_num_of_balls ];
}

has history => (
    is         => "rw",
    isa        => "HashRef",
    lazy_build => 1,
);

sub _build_history { {} }

no Mouse;

sub next_ball {
    my $self         = shift;
    my $current_ball = $self->_current_ball + 1;
    my $ball         = $self->_balls->[$current_ball]
      or return;
    $self->_current_ball($current_ball);
    return $ball;
}

sub roll_ball {
    my $self = shift;
    my $ball = $self->next_ball
      or return;
    my $rolled_distance = random_uniform_integer( 1, 10, 50 );
    $self->history->{$ball} = $rolled_distance;
    return $rolled_distance;
}

1;
