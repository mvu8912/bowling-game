package Bowling::Game;

use strict;
use warnings;
use IO::Prompt qw( prompt );
use Math::Random qw( random_uniform_integer );
use Bowling::Round;

=head1 NAME

Bowling::Game - Bowling Game Simulation

=cut

use Mouse;

has _rounds => (
    is         => "rw",
    isa        => "ArrayRef[Bowling::Round]",
    lazy_build => 1,
);

sub _build__rounds {
    my $self           = shift;
    my $num_of_rounds  = $self->_num_of_rounds;
    my $num_of_players = $self->_num_of_players;
    my $num_of_balls   = $self->_num_of_balls;

    my @rounds = map {
        Bowling::Round->new(
            id             => $_,
            num_of_players => $num_of_players,
            num_of_balls   => $num_of_balls,
          )
    } ( 1 .. $num_of_rounds );

    return \@rounds;
}

has _num_of_players => (
    is  => "rw",
    isa => "Int",
);

has _num_of_balls => (
    is  => "rw",
    isa => "Int",
);

has _num_of_rounds => (
    is  => "rw",
    isa => "Int",
);

has _field_size => (
    is         => "rw",
    isa        => "Int",
    lazy_build => 1
);

sub _build__field_size {
    return random_uniform_integer( 1, 10, 50 );
}

has _jack_location => (
    is         => "rw",
    isa        => "Int",
    lazy_build => 1
);

sub _build__jack_location {
    my $self       = shift;
    my $field_size = $self->_field_size;
    my @range      = ( $field_size - 5, $field_size - 2 );
    return random_uniform_integer( 1, @range );
}

has _current_round => (
    is      => "rw",
    isa     => "Int",
    default => -1,
);

no Mouse;

=head1 METHODS

=head2 ask_num_of_players

Ask question to get number of players

=cut

sub ask_num_of_players {
    my $self  = shift;
    my $total = prompt(
        "How many player?",
        -default => 5,
        -while   => qr/^[2-9][1-9]*/,
        "-integer",
    );
    $self->_num_of_players( ~~ $total );
    return $self;
}

sub ask_num_of_balls {
    my $self = shift;
    my $balls =
      prompt( "How many balls for each player?", "-integer", -default => 3 );
    $self->_num_of_balls( ~~ $balls );
    return $self;
}

sub ask_num_of_rounds {
    my $self = shift;
    my $rounds =
      prompt( "How many rounds of the game?", "-integer", -default => 2 );
    $self->_num_of_rounds( ~~ $rounds );
    return $self;
}

sub set_field {
    my $self = shift;
    $self->_field_size;
    return $self;
}

sub set_jack_position {
    my $self = shift;
    $self->_jack_location;
    return $self;
}

sub set_rounds {
    my $self = shift;
    $self->_rounds;
    return $self;
}

sub next_round {
    my $self          = shift;
    my $current_round = $self->_current_round + 1;
    my $round         = $self->_rounds->[$current_round]
      or return;
    $self->_current_round($current_round);
    return $round;
}

sub result_of_each_players {
    my $self          = shift;
    my $field_size    = $self->_field_size;
    my $jack_location = $self->_jack_location;

    my %agg_result = ();

    foreach my $round( @{ $self->_rounds } ) {
        foreach my $player ( @{ $round->players } ) {
            my $points = 0;
            my %history = %{$player->history};
            foreach my $ball_id ( keys %history ) {
                my $distance = $history{$ball_id};

                ## Out of range -- too far
                if ( $distance > $field_size ) {
                    $points += 0;
                }
                ## Not in range
                elsif ( $distance < 10 ) {
                    $points += 0;
                }
                elsif ( $distance == $jack_location ) {
                    $points += 1;
                }
                ## very close
                elsif ( $distance == $jack_location - 1 ) {
                    $points += 1;
                }
                ## very close
                elsif ( $distance == $jack_location + 1 ) {
                    $points += 1;
                }
                ## very close
                elsif ( $distance == $jack_location - 2 ) {
                    $points += 1;
                }
                ## very close
                elsif ( $distance == $jack_location + 2 ) {
                    $points += 1;
                }
            }
            $agg_result{$player->name}{name} = $player->name;
            $agg_result{$player->name}{points} += $points;
        }
    }

    my @sorted_by_highest_score =
      map  { $agg_result{$_} }
      sort { $agg_result{$b}{points} <=> $agg_result{$a}{points} }
      keys %agg_result;

    return @sorted_by_highest_score;
}

1;
